package ru.aisa.coffeeMaker.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aisa.coffeeMaker.models.ParametersCoffeeMaker;
import ru.aisa.coffeeMaker.models.ResourcesCoffeeMaker;
import ru.aisa.coffeeMaker.repositories.ResourcesCoffeeMakerRepository;

import java.util.Comparator;

@Service
@Transactional(readOnly = true)
public class ResourcesCoffeeMakerServiceImpl implements ResourcesCoffeeMakerService {

    private final ResourcesCoffeeMakerRepository resourcesCoffeeMakerRepository;
    private final ControlCoffeeMakerService controlCoffeeMakerService;

    @Autowired
    public ResourcesCoffeeMakerServiceImpl(ResourcesCoffeeMakerRepository resourcesCoffeeMakerRepository, ControlCoffeeMakerService controlCoffeeMakerService) {
        this.resourcesCoffeeMakerRepository = resourcesCoffeeMakerRepository;
        this.controlCoffeeMakerService = controlCoffeeMakerService;
    }

    @Override
    public ResourcesCoffeeMaker getResourcesCoffeeMaker(int idCoffeeMaker) {
        return resourcesCoffeeMakerRepository.findByControlCoffeeMaker(
                        controlCoffeeMakerService.getTheCurrentState(idCoffeeMaker))
                .stream().max(Comparator.comparingInt(ResourcesCoffeeMaker::getIdResources)).get();
    }
}
