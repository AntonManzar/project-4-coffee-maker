package ru.aisa.coffeeMaker.services;

import ru.aisa.coffeeMaker.models.ResourcesCoffeeMaker;

public interface ResourcesCoffeeMakerService {

    /**
     * Возвращает информацию о колличестве текущих ресурсов.
     * @param idCoffeeMaker - уникальный идентификатор кофеварки.
     * @return возвращает объект ResourcesCoffeeMaker.
     */
    ResourcesCoffeeMaker getResourcesCoffeeMaker(int idCoffeeMaker);
}
