package ru.aisa.coffeeMaker.services;

import ru.aisa.coffeeMaker.models.ControlCoffeeMaker;

public interface ControlCoffeeMakerService {

    /**
     * Возвращает текущее состояние выбранной кофеварки.
     * @param idCoffeeMaker - уникальный идентификатор кофеварки.
     * @return возвращает объект ControlCoffeeMaker.
     */
    ControlCoffeeMaker getTheCurrentState(int idCoffeeMaker);

    /**
     * Изменяет текущее состояние выбранной кофеварки.
     * @param idCoffeeMaker - уникальный идентификатор кофеварки.
     * @param controlCoffeeMaker - объект, поля которого необходимо сохранить как изменения.
     */
    void editTheCurrentState(int idCoffeeMaker, ControlCoffeeMaker controlCoffeeMaker);
}
