package ru.aisa.coffeeMaker.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aisa.coffeeMaker.errors.ControlCoffeeMakerNotFoundException;
import ru.aisa.coffeeMaker.models.ControlCoffeeMaker;
import ru.aisa.coffeeMaker.repositories.ControlCoffeeMakerRepository;

import java.util.Optional;

@Service
@Transactional(readOnly = true)
public class ControlCoffeeMakerServicesImpl implements ControlCoffeeMakerService {

    private final ControlCoffeeMakerRepository controlCoffeeMakerRepository;

    @Autowired
    public ControlCoffeeMakerServicesImpl(ControlCoffeeMakerRepository controlCoffeeMakerRepository) {
        this.controlCoffeeMakerRepository = controlCoffeeMakerRepository;
    }

    @Override
    public ControlCoffeeMaker getTheCurrentState(int idCoffeeMaker) {
        Optional<ControlCoffeeMaker> foundControlCoffeeMaker = controlCoffeeMakerRepository.findById(idCoffeeMaker);
        return foundControlCoffeeMaker.orElseThrow(ControlCoffeeMakerNotFoundException::new);
    }

    @Override
    @Transactional
    public void editTheCurrentState(int idCoffeeMaker, ControlCoffeeMaker newParamForControlCoffeeMaker) {
        ControlCoffeeMaker updatedControlCoffeeMaker = controlCoffeeMakerRepository.findById(idCoffeeMaker).
                orElseThrow(ControlCoffeeMakerNotFoundException::new);
        updatedControlCoffeeMaker.setWorkingMode(newParamForControlCoffeeMaker.getWorkingMode());
        updatedControlCoffeeMaker.setTimerAutoOn(newParamForControlCoffeeMaker.getTimerAutoOn());
        updatedControlCoffeeMaker.setNameCoffeeMaker(newParamForControlCoffeeMaker.getNameCoffeeMaker());
        controlCoffeeMakerRepository.save(updatedControlCoffeeMaker);
    }

}
