package ru.aisa.coffeeMaker.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.aisa.coffeeMaker.models.ParametersCoffeeMaker;
import ru.aisa.coffeeMaker.repositories.ParametersCoffeeMakerRepository;

import java.util.Comparator;
import java.util.Date;

@Service
@Transactional(readOnly = true)
public class ParametersCoffeeMakerServiceImpl implements ParametersCoffeeMakerService{

    private final ParametersCoffeeMakerRepository parametersCoffeeMakerRepository;
    private final ControlCoffeeMakerService controlCoffeeMakerService;

    @Autowired
    public ParametersCoffeeMakerServiceImpl(ParametersCoffeeMakerRepository parametersCoffeeMakerRepository, ControlCoffeeMakerService controlCoffeeMakerService) {
        this.parametersCoffeeMakerRepository = parametersCoffeeMakerRepository;
        this.controlCoffeeMakerService = controlCoffeeMakerService;
    }

    @Override
    public ParametersCoffeeMaker getParametersCoffeeMaker(int idCoffeeMaker) {
        return parametersCoffeeMakerRepository.findByControlCoffeeMaker(
                controlCoffeeMakerService.getTheCurrentState(idCoffeeMaker))
                .stream().max(Comparator.comparingInt(ParametersCoffeeMaker::getIdParameters)).get();
    }

    @Override
    @Transactional
    public void setNewParametersCoffeeMaker(int idCoffeeMaker, ParametersCoffeeMaker parametersCoffeeMaker) {
        parametersCoffeeMaker.setControlCoffeeMaker(controlCoffeeMakerService.getTheCurrentState(idCoffeeMaker));
        parametersCoffeeMaker.setDateOfChangeParameters(new Date());
        parametersCoffeeMakerRepository.save(parametersCoffeeMaker);
    }
}
