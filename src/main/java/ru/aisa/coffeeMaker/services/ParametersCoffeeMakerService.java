package ru.aisa.coffeeMaker.services;

import ru.aisa.coffeeMaker.models.ParametersCoffeeMaker;

public interface ParametersCoffeeMakerService {

    /**
     * Возвращает текущие настройки кофеварки.
     * @param idCoffeeMaker - уникальный идентификатор кофеварки.
     * @return возвращает объект ParametersCoffeeMaker.
     */
    ParametersCoffeeMaker getParametersCoffeeMaker(int idCoffeeMaker);

    /**
     * Сохраняет текущие настройки кофеварки
     * @param idCoffeeMaker - уникальный идентификатор кофеварки.
     * @param parametersCoffeeMaker - полученные настройки для сохранения.
     */
    void setNewParametersCoffeeMaker(int idCoffeeMaker, ParametersCoffeeMaker parametersCoffeeMaker);
}
