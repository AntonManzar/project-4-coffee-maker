package ru.aisa.coffeeMaker.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@ApiModel(description = "DTO для отображения ресурсов кофеварки")
public class ResourcesCoffeeMakerDTO {


    @Min(value = 0, message = "Минимальное значение запаса кофе 0 в % от общего объема хранилища")
    @Max(value = 100, message = "Максимальное запаса кофе значение 100 в % от общего объема хранилища")
    @ApiModelProperty(value = "Колицество сырья для кофе относительно объёма емкости в процентах", example = "85")
    private int amountOfCoffee;

    @Min(value = 0, message = "Минимальное значение запаса воды 0 в % от общего объема хранилища")
    @Max(value = 100, message = "Максимальное значение запаса воды 100 в % от общего объема хранилища")
    @ApiModelProperty(value = "Колицество воды относительно объёма емкости в процентах", example = "90")
    private int amountOfWater;

    @Min(value = 0, message = "Минимальное значение запаса воды 0 в % произведенного кофе от общего объема")
    @Max(value = 400, message = "Максимальное значение запаса воды 400 в % произведенного кофе от общего объема")
    @ApiModelProperty(value = "Счетчик до чистки кофеварки. Расчет в процентах от ипользуемой емкости.", example = "350")
    private int cyclesBeforeCleaning;

    public int getAmountOfCoffee() {
        return amountOfCoffee;
    }

    public void setAmountOfCoffee(int amountOfCoffee) {
        this.amountOfCoffee = amountOfCoffee;
    }

    public int getAmountOfWater() {
        return amountOfWater;
    }

    public void setAmountOfWater(int amountOfWater) {
        this.amountOfWater = amountOfWater;
    }

    public int getCyclesBeforeCleaning() {
        return cyclesBeforeCleaning;
    }

    public void setCyclesBeforeCleaning(int cyclesBeforeCleaning) {
        this.cyclesBeforeCleaning = cyclesBeforeCleaning;
    }
}
