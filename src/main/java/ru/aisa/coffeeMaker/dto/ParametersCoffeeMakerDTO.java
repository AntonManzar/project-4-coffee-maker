package ru.aisa.coffeeMaker.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import ru.aisa.coffeeMaker.models.enums.Strength;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@ApiModel(description = "DTO для управления параметрами кофеварки.")
public class ParametersCoffeeMakerDTO {

    @NotNull
    @ApiModelProperty(value = "Крепость кофе", example = "STANDARD")
    private Strength strengthOfBrewedCoffee;

    @Min(value = 60, message = "Минимальная температура для нагрева 60")
    @Max(value = 95, message = "Максимальная температура нагрева 95")
    @ApiModelProperty(value = "Температура подогрева в градусах", example = "90")
    private int heatingTemperature;

    @Min(value = 0, message = "Минимальное значение производимого кофе 0 в % от общего объема")
    @Max(value = 100, message = "Максимальное значение производимого кофе 100 в % от общего объема")
    @ApiModelProperty(value = "Объём производимого кофе относительно объема емкости в процентах", example = "50")
    private int volumeOfCoffeeProduced;

    public Strength getStrengthOfBrewedCoffee() {
        return strengthOfBrewedCoffee;
    }

    public void setStrengthOfBrewedCoffee(Strength strengthOfBrewedCoffee) {
        this.strengthOfBrewedCoffee = strengthOfBrewedCoffee;
    }

    public int getHeatingTemperature() {
        return heatingTemperature;
    }

    public void setHeatingTemperature(int heatingTemperature) {
        this.heatingTemperature = heatingTemperature;
    }

    public int getVolumeOfCoffeeProduced() {
        return volumeOfCoffeeProduced;
    }

    public void setVolumeOfCoffeeProduced(int volumeOfCoffeeProduced) {
        this.volumeOfCoffeeProduced = volumeOfCoffeeProduced;
    }
}
