package ru.aisa.coffeeMaker.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@ApiModel(description = "DTO для управления кофеваркой")
public class ControlCoffeeMakerDTO {

    @NotEmpty(message = "Имя не может быть пустым.")
    @Size(min = 4,max = 50, message = "Имя должно содержать от 4 до 50 символов.")
    @ApiModelProperty(value = "Имя кофеварки", example = "CM007")
    private String nameCoffeeMaker;

    @Min(value = 0, message = "Значение не может быть отрицательным.")
    @Max(value = 86400000, message = "Занечени выходит за границы допустимых значений.!")
    @ApiModelProperty(value = "Таймер автоматического включения", example = "30000000")
    private int timerAutoOn;

    @ApiModelProperty(value = "Положение: вкл./выкл.", example = "true")
    private boolean workingMode;

    public String getNameCoffeeMaker() {
        return nameCoffeeMaker;
    }

    public void setNameCoffeeMaker(String nameCoffeeMaker) {
        this.nameCoffeeMaker = nameCoffeeMaker;
    }

    public int getTimerAutoOn() {
        return timerAutoOn;
    }

    public void setTimerAutoOn(int timerAutoOn) {
        this.timerAutoOn = timerAutoOn;
    }

    public boolean isWorkingMode() {
        return workingMode;
    }

    public void setWorkingMode(boolean workingMode) {
        this.workingMode = workingMode;
    }

    @Override
    public String toString() {
        return "ControlCoffeeMakerDTO{" +
                "nameCoffeeMaker='" + nameCoffeeMaker + '\'' +
                ", timerAutoOn=" + timerAutoOn +
                ", workingMode=" + workingMode +
                '}';
    }
}
