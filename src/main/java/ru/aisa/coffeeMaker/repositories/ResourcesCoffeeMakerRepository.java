package ru.aisa.coffeeMaker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.aisa.coffeeMaker.models.ControlCoffeeMaker;
import ru.aisa.coffeeMaker.models.ResourcesCoffeeMaker;

import java.util.List;

@Repository
public interface ResourcesCoffeeMakerRepository extends JpaRepository<ResourcesCoffeeMaker, Integer> {

    /**
     * Поиск объектов ResourcesCoffeeMaker, принадлежащих объекту ControlCoffeeMaker.
     * @param controlCoffeeMaker - целевой объект для поиска.
     * @return возвращает список объектов ResourcesCoffeeMaker.
     */
    List<ResourcesCoffeeMaker> findByControlCoffeeMaker(ControlCoffeeMaker controlCoffeeMaker);
}
