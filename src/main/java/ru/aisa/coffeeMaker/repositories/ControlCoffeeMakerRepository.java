package ru.aisa.coffeeMaker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.aisa.coffeeMaker.models.ControlCoffeeMaker;

@Repository
public interface ControlCoffeeMakerRepository extends JpaRepository<ControlCoffeeMaker, Integer> {
}
