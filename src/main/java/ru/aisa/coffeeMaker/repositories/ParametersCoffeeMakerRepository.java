package ru.aisa.coffeeMaker.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.aisa.coffeeMaker.models.ControlCoffeeMaker;
import ru.aisa.coffeeMaker.models.ParametersCoffeeMaker;

import java.util.List;

@Repository
public interface ParametersCoffeeMakerRepository extends JpaRepository<ParametersCoffeeMaker, Integer> {

    /**
     * Поиск объектов ParametersCoffeeMaker, принадлежащих объекту ControlCoffeeMaker.
     * @param controlCoffeeMaker - целевой объект для поиска.
     * @return возвращает список объектов ParametersCoffeeMaker.
     */
    List<ParametersCoffeeMaker> findByControlCoffeeMaker(ControlCoffeeMaker controlCoffeeMaker);
}
