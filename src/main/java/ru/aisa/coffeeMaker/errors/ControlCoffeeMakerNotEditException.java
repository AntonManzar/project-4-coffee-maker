package ru.aisa.coffeeMaker.errors;

public class ControlCoffeeMakerNotEditException extends RuntimeException {

    public ControlCoffeeMakerNotEditException(String message) {
        super(message);
    }
}
