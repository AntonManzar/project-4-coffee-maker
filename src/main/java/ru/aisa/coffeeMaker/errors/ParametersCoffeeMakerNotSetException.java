package ru.aisa.coffeeMaker.errors;

public class ParametersCoffeeMakerNotSetException extends RuntimeException {

    public ParametersCoffeeMakerNotSetException(String message) {
        super(message);
    }
}
