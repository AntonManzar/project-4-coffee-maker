package ru.aisa.coffeeMaker.models;

import ru.aisa.coffeeMaker.models.enums.Strength;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "parameters_coffee_maker")
public class ParametersCoffeeMaker {

    @Id
    @Column(name = "id_parameters")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idParameters;

    @Column(name = "strength_of_brewed_coffee")
    @Enumerated(EnumType.STRING)
    private Strength strengthOfBrewedCoffee;

    @Column(name = "heating_temperature")
    private int heatingTemperature;

    @Column(name = "volume_of_coffee_produced")
    private int volumeOfCoffeeProduced;

    @Column(name = "date_of_change_parameters")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfChangeParameters;

    @ManyToOne
    @JoinColumn(name = "id_control", referencedColumnName = "id_control")
    private ControlCoffeeMaker controlCoffeeMaker;

    public ParametersCoffeeMaker() {
    }

    public int getIdParameters() {
        return idParameters;
    }

    public void setIdParameters(int idParameters) {
        this.idParameters = idParameters;
    }

    public Strength getStrengthOfBrewedCoffee() {
        return strengthOfBrewedCoffee;
    }

    public void setStrengthOfBrewedCoffee(Strength strengthOfBrewedCoffee) {
        this.strengthOfBrewedCoffee = strengthOfBrewedCoffee;
    }

    public int getHeatingTemperature() {
        return heatingTemperature;
    }

    public void setHeatingTemperature(int heatingTemperature) {
        this.heatingTemperature = heatingTemperature;
    }

    public int getVolumeOfCoffeeProduced() {
        return volumeOfCoffeeProduced;
    }

    public void setVolumeOfCoffeeProduced(int volumeOfCoffeeProduced) {
        this.volumeOfCoffeeProduced = volumeOfCoffeeProduced;
    }

    public Date getDateOfChangeParameters() {
        return dateOfChangeParameters;
    }

    public void setDateOfChangeParameters(Date dateOfChangeParameters) {
        this.dateOfChangeParameters = dateOfChangeParameters;
    }

    public ControlCoffeeMaker getControlCoffeeMaker() {
        return controlCoffeeMaker;
    }

    public void setControlCoffeeMaker(ControlCoffeeMaker controlCoffeeMaker) {
        this.controlCoffeeMaker = controlCoffeeMaker;
    }
}
