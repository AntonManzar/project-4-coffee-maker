package ru.aisa.coffeeMaker.models;

import javax.persistence.*;
import java.util.List;


@Entity
@Table(name = "control_coffee_maker")
public class ControlCoffeeMaker {

    @Id
    @Column(name = "id_control")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idControl;

    @Column(name = "name_coffee_maker")
    private String nameCoffeeMaker;

    @Column(name = "timer_auto_on")
    private int timerAutoOn;

    @Column(name = "working_mode")
    private boolean workingMode;

    @OneToMany(mappedBy = "controlCoffeeMaker")
    private List<ParametersCoffeeMaker> parametersCoffeeMakerList;

    @OneToMany(mappedBy = "controlCoffeeMaker")
    private List<ResourcesCoffeeMaker> resourcesCoffeeMakers;

    public ControlCoffeeMaker() {
    }

    public int getIdControl() {
        return idControl;
    }

    public void setIdControl(int idControl) {
        this.idControl = idControl;
    }

    public String getNameCoffeeMaker() {
        return nameCoffeeMaker;
    }

    public void setNameCoffeeMaker(String nameCoffeeMaker) {
        this.nameCoffeeMaker = nameCoffeeMaker;
    }

    public int getTimerAutoOn() {
        return timerAutoOn;
    }

    public void setTimerAutoOn(int timerAutoOn) {
        this.timerAutoOn = timerAutoOn;
    }

    public boolean getWorkingMode() {
        return workingMode;
    }

    public void setWorkingMode(boolean workingMode) {
        this.workingMode = workingMode;
    }

    public List<ParametersCoffeeMaker> getParametersCoffeeMakerList() {
        return parametersCoffeeMakerList;
    }

    public void setParametersCoffeeMakerList(List<ParametersCoffeeMaker> parametersCoffeeMakerList) {
        this.parametersCoffeeMakerList = parametersCoffeeMakerList;
    }

    public List<ResourcesCoffeeMaker> getResourcesCoffeeMakers() {
        return resourcesCoffeeMakers;
    }

    public void setResourcesCoffeeMakers(List<ResourcesCoffeeMaker> resourcesCoffeeMakers) {
        this.resourcesCoffeeMakers = resourcesCoffeeMakers;
    }
}
