package ru.aisa.coffeeMaker.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "resources_coffee_maker")
public class ResourcesCoffeeMaker {

    @Id
    @Column(name = "id_resources")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idResources;

    @Column(name = "amount_of_coffee")
    private int amountOfCoffee;

    @Column(name = "amount_of_water")
    private int amountOfWater;

    @Column(name = "cycles_before_cleaning")
    private int cyclesBeforeCleaning;

    @Column(name = "date_of_change_resources")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfChangeResources;

    @ManyToOne
    @JoinColumn(name = "id_control", referencedColumnName = "id_control")
    private ControlCoffeeMaker controlCoffeeMaker;

    public ResourcesCoffeeMaker() {
    }

    public int getIdResources() {
        return idResources;
    }

    public void setIdResources(int idResources) {
        this.idResources = idResources;
    }

    public int getAmountOfCoffee() {
        return amountOfCoffee;
    }

    public void setAmountOfCoffee(int amountOfCoffee) {
        this.amountOfCoffee = amountOfCoffee;
    }

    public int getAmountOfWater() {
        return amountOfWater;
    }

    public void setAmountOfWater(int amountOfWater) {
        this.amountOfWater = amountOfWater;
    }

    public int getCyclesBeforeCleaning() {
        return cyclesBeforeCleaning;
    }

    public void setCyclesBeforeCleaning(int cyclesBeforeCleaning) {
        this.cyclesBeforeCleaning = cyclesBeforeCleaning;
    }

    public Date getDateOfChangeResources() {
        return dateOfChangeResources;
    }

    public void setDateOfChangeResources(Date dateOfChangeResources) {
        this.dateOfChangeResources = dateOfChangeResources;
    }

    public ControlCoffeeMaker getControlCoffeeMaker() {
        return controlCoffeeMaker;
    }

    public void setControlCoffeeMaker(ControlCoffeeMaker controlCoffeeMaker) {
        this.controlCoffeeMaker = controlCoffeeMaker;
    }
}
