package ru.aisa.coffeeMaker.models.enums;

public enum Strength {
    VERY_WEAK,WEAK,STANDARD,STRONG,VERY_STRONG
}
