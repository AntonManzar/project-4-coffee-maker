package ru.aisa.coffeeMaker.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import ru.aisa.coffeeMaker.dto.ParametersCoffeeMakerDTO;
import ru.aisa.coffeeMaker.errors.CoffeeMakerErrorResponse;
import ru.aisa.coffeeMaker.errors.ControlCoffeeMakerNotFoundException;
import ru.aisa.coffeeMaker.errors.ParametersCoffeeMakerNotSetException;
import ru.aisa.coffeeMaker.models.ParametersCoffeeMaker;
import ru.aisa.coffeeMaker.services.ParametersCoffeeMakerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/parameters")
@Api(description = "Контроллер управления текущими настройками кофеварки.")
public class ParametersCoffeeMakerController {

    private final ParametersCoffeeMakerService parametersCoffeeMakerService;
    private final ModelMapper modelMapper;

    public ParametersCoffeeMakerController(ParametersCoffeeMakerService parametersCoffeeMakerService, ModelMapper modelMapper) {
        this.parametersCoffeeMakerService = parametersCoffeeMakerService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение текущих настроек кофеварки.")
    public ParametersCoffeeMakerDTO getParametersCoffeeMaker(@PathVariable("id") int idCoffeeMaker) {
        return convertToParametersCoffeeMakerDTO(parametersCoffeeMakerService.getParametersCoffeeMaker(idCoffeeMaker));
    }

    @PostMapping("/{id}")
    @ApiOperation("Принять и сохранить последние настройки кофеварки.")
    public ResponseEntity<HttpStatus> setNewParametersCoffeeMaker(
            @PathVariable("id") int idCoffeeMaker,
            @RequestBody @Valid ParametersCoffeeMakerDTO parametersCoffeeMakerDTO,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            StringBuilder errorMessage = new StringBuilder();
            List<FieldError> errorsList = bindingResult.getFieldErrors();
            for (FieldError error : errorsList) {
                errorMessage.append(error.getField())
                        .append(" - ")
                        .append(error.getDefaultMessage())
                        .append(";");
            }
            throw new ParametersCoffeeMakerNotSetException(errorMessage.toString());
        }
        parametersCoffeeMakerService.setNewParametersCoffeeMaker(idCoffeeMaker, convertToParametersCoffeeMaker(parametersCoffeeMakerDTO));
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ExceptionHandler
    private ResponseEntity<CoffeeMakerErrorResponse> handleException(ControlCoffeeMakerNotFoundException error) {
        CoffeeMakerErrorResponse response = new CoffeeMakerErrorResponse(
                "Кофеварка с таким id не найдена!",
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<CoffeeMakerErrorResponse> handleException(ParametersCoffeeMakerNotSetException error) {
        CoffeeMakerErrorResponse response = new CoffeeMakerErrorResponse(
                error.getMessage(),
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    private ParametersCoffeeMaker convertToParametersCoffeeMaker(ParametersCoffeeMakerDTO parametersCoffeeMakerDTO) {
        return modelMapper.map(parametersCoffeeMakerDTO, ParametersCoffeeMaker.class);
    }

    private ParametersCoffeeMakerDTO convertToParametersCoffeeMakerDTO(ParametersCoffeeMaker parametersCoffeeMaker) {
        return modelMapper.map(parametersCoffeeMaker, ParametersCoffeeMakerDTO.class);
    }
}
