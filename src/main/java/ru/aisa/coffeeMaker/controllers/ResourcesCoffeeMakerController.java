package ru.aisa.coffeeMaker.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.aisa.coffeeMaker.dto.ResourcesCoffeeMakerDTO;
import ru.aisa.coffeeMaker.errors.CoffeeMakerErrorResponse;
import ru.aisa.coffeeMaker.errors.ControlCoffeeMakerNotFoundException;
import ru.aisa.coffeeMaker.models.ResourcesCoffeeMaker;
import ru.aisa.coffeeMaker.services.ResourcesCoffeeMakerService;

@RestController
@RequestMapping("/resources")
@Api(description = "Контроллер для отображения текущих ресурсов")
public class ResourcesCoffeeMakerController {

    private final ResourcesCoffeeMakerService resourcesCoffeeMakerService;
    private final ModelMapper modelMapper;

    @Autowired
    public ResourcesCoffeeMakerController(ResourcesCoffeeMakerService resourcesCoffeeMakerService, ModelMapper modelMapper) {
        this.resourcesCoffeeMakerService = resourcesCoffeeMakerService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение текущих ресурсов кофеварки")
    public ResourcesCoffeeMakerDTO getResourcesCoffeeMaker(@PathVariable("id") int idCoffeeMaker) {
        return convertToResourcesCoffeeMakerDTO(resourcesCoffeeMakerService.getResourcesCoffeeMaker(idCoffeeMaker));
    }
    
    @ExceptionHandler
    private ResponseEntity<CoffeeMakerErrorResponse> handleException(ControlCoffeeMakerNotFoundException error) {
        CoffeeMakerErrorResponse response = new CoffeeMakerErrorResponse(
                "Кофеварка с таким id не найдена!",
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    private ResourcesCoffeeMaker convertToResourcesCoffeeMaker(ResourcesCoffeeMakerDTO resourcesCoffeeMakerDTO) {
        return modelMapper.map(resourcesCoffeeMakerDTO, ResourcesCoffeeMaker.class);
    }

    private ResourcesCoffeeMakerDTO convertToResourcesCoffeeMakerDTO(ResourcesCoffeeMaker resourcesCoffeeMaker) {
        return modelMapper.map(resourcesCoffeeMaker, ResourcesCoffeeMakerDTO.class);
    }
}
