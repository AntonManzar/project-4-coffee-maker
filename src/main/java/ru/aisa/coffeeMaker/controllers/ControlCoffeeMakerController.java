package ru.aisa.coffeeMaker.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import ru.aisa.coffeeMaker.dto.ControlCoffeeMakerDTO;
import ru.aisa.coffeeMaker.errors.CoffeeMakerErrorResponse;
import ru.aisa.coffeeMaker.errors.ControlCoffeeMakerNotEditException;
import ru.aisa.coffeeMaker.errors.ControlCoffeeMakerNotFoundException;
import ru.aisa.coffeeMaker.models.ControlCoffeeMaker;
import ru.aisa.coffeeMaker.services.ControlCoffeeMakerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/control")
@Api(description = "Контроллер для проверки и управления текущим состоянием кофеварки.")
public class ControlCoffeeMakerController {

    private final ControlCoffeeMakerService controlCoffeeMakerService;
    private final ModelMapper modelMapper;

    @Autowired
    public ControlCoffeeMakerController(ControlCoffeeMakerService controlCoffeeMakerService, ModelMapper modelMapper) {
        this.controlCoffeeMakerService = controlCoffeeMakerService;
        this.modelMapper = modelMapper;
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение текущего состояния кофеварки, списки")
    public ControlCoffeeMakerDTO getTheCurrentState(@PathVariable("id") int idCoffeeMaker) {
        return convertToControlCoffeeMakerDTO(controlCoffeeMakerService.getTheCurrentState(idCoffeeMaker));
    }

    @PatchMapping("/{id}")
    @ApiOperation("Изменение текущего состояния кофеварки.")
    public ResponseEntity<HttpStatus> editTheCurrentState(
            @PathVariable("id") int idCoffeeMaker,
            @RequestBody @Valid ControlCoffeeMakerDTO controlCoffeeMakerDTO,
            BindingResult bindingResult) {

        if (bindingResult.hasErrors()) {
            StringBuilder errorMessage = new StringBuilder();
            List<FieldError> errorsList = bindingResult.getFieldErrors();
            for (FieldError error : errorsList) {
                errorMessage.append(error.getField())
                        .append(" - ")
                        .append(error.getDefaultMessage())
                        .append(";");
            }
            throw new ControlCoffeeMakerNotEditException(errorMessage.toString());
        }
        controlCoffeeMakerService.editTheCurrentState(idCoffeeMaker, convertToControlCoffeeMaker(controlCoffeeMakerDTO));
        return ResponseEntity.ok(HttpStatus.OK);
    }

    @ExceptionHandler
    private ResponseEntity<CoffeeMakerErrorResponse> handleException(ControlCoffeeMakerNotFoundException error) {
        CoffeeMakerErrorResponse response = new CoffeeMakerErrorResponse(
                "Кофеварка с таким id не найдена!",
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    private ResponseEntity<CoffeeMakerErrorResponse> handleException(ControlCoffeeMakerNotEditException error) {
        CoffeeMakerErrorResponse response = new CoffeeMakerErrorResponse(
                error.getMessage(),
                System.currentTimeMillis()
        );
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    private ControlCoffeeMaker convertToControlCoffeeMaker(ControlCoffeeMakerDTO controlCoffeeMakerDTO) {
        return modelMapper.map(controlCoffeeMakerDTO, ControlCoffeeMaker.class);
    }

    private ControlCoffeeMakerDTO convertToControlCoffeeMakerDTO(ControlCoffeeMaker controlCoffeeMaker) {
        return modelMapper.map(controlCoffeeMaker, ControlCoffeeMakerDTO.class);
    }
}
